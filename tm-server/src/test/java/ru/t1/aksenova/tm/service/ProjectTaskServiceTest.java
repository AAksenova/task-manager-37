package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
@Ignore
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);


    @Before
    public void before() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
        taskService.add(USER_TASK1);
        taskService.add(USER_TASK2);
        taskService.add(USER_TASK3);
    }

    @After
    public void after() {
        taskService.removeAll();
        projectService.removeAll();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskService.findOneById(USER_TEST.getId(), USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskService.findOneById(USER_TEST.getId(), USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(null, USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));

        @NotNull List<Task> tasks = taskService.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        projectTaskService.removeTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertThrows(AbstractException.class, () -> taskService.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

}

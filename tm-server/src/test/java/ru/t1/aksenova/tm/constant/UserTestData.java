package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.service.PropertyService;
import ru.t1.aksenova.tm.util.HashUtil;

import java.util.UUID;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static User USER_TEST = new User();

    @NotNull
    public final static User ADMIN_TEST = new User();

    @NotNull
    public final static String USER_TEST_LOGIN = "test2";

    @NotNull
    public final static String USER_TEST_PASSWORD = "test2";

    @NotNull
    public final static String USER_TEST_EMAIL = "test2@test.com";

    @NotNull
    public final static String USER_ADMIN_LOGIN = "admin2";

    @NotNull
    public final static String USER_ADMIN_PASSWORD = "admin2";

    @NotNull
    public final static String USER_ADMIN_EMAIL = "admin2@test.com";

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    public final static String NON_EXISTING_LOGIN = "LoginNoExist";

    @NotNull
    public final static String NON_EXISTING_EMAIL = "EmailNoExist@test.com";

    @NotNull
    public final static String NON_EXISTING_PASSWORD = "LoginNoExist";

    @NotNull
    public final static String FIRST_NAME = "Test First Name";

    @NotNull
    public final static String LAST_NAME = "Test Last Name";

    @NotNull
    public final static String MIDDLE_NAME = "Test Middle Name";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(propertyService, USER_TEST_PASSWORD));
        USER_TEST.setEmail(USER_TEST_EMAIL);

        ADMIN_TEST.setLogin(USER_ADMIN_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(propertyService, USER_ADMIN_PASSWORD));
        ADMIN_TEST.setEmail(USER_ADMIN_EMAIL);
    }

}

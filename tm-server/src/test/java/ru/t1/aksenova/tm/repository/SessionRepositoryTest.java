package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.SessionTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ISessionRepository repository = new SessionRepository(connection);

    @Before
    public void before() {
        repository.add(USER_TEST.getId(), USER_SESSION_TEST);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_SESSION_TEST));
        @Nullable final Session session = repository.findOneById(ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void findAll() {
        repository.removeAll();
        @NotNull final ISessionRepository emptyRepository = new SessionRepository(connection);
        emptyRepository.add(SESSION_LIST);
        final List<Session> sessions = emptyRepository.findAll(USER_TEST.getId());
        sessions.forEach(session -> Assert.assertEquals(USER_TEST.getId(), session.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Session> sessions = repository.findAll(USER_TEST.getId());
        sessions.forEach(session -> Assert.assertEquals(USER_TEST.getId(), session.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_TEST.getId(), USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_SESSION_TEST.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_SESSION_TEST.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository(connection);
        emptyRepository.add(SESSION_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Session session = repository.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
        repository.removeOne(session);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
        repository.removeOneById(session.getId());
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_SESSION_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_SESSION_TEST.getId()));
        repository.add(ADMIN_SESSION_TEST);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION_TEST.getId(), ADMIN_SESSION_TEST.getId()));
    }

}

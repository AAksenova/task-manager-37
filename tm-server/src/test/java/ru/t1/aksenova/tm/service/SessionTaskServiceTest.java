package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ISessionRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.ISessionService;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.repository.SessionRepository;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.SessionTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
@Ignore
public final class SessionTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @Before
    public void before() {
        service.add(USER_SESSION_TEST);
    }

    @After
    public void after() {
        service.removeAll();
    }

    @Test
    public void add() {
        Assert.assertThrows(AbstractException.class, () -> service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION_TEST));
        @Nullable final Session session = service.findOneById(ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(ADMIN_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void addMany() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ISessionService emptyService = new SessionService(connectionService);
        Assert.assertNotNull(emptyService.add(SESSION_LIST));
        for (final Session session : SESSION_LIST)
            Assert.assertEquals(session.getId(), emptyService.findOneById(session.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, ADMIN_SESSION_TEST));
        Assert.assertThrows(AbstractException.class, () -> service.add("", ADMIN_SESSION_TEST));
        Assert.assertThrows(AbstractException.class, () -> service.add(ADMIN_TEST.getId(), NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_SESSION_TEST));
        @Nullable final Session session = service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(ADMIN_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void set() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ISessionService emptyService = new SessionService(connectionService);
        emptyService.set(SESSION_LIST);
        final List<Session> sessions = emptyService.findAll();
        Assert.assertEquals(SESSION_LIST.size(), sessions.size());
    }

    @Test
    public void findAll() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ISessionService emptyService = new SessionService(connectionService);
        final int countRecord = emptyService.findAll().size();
        emptyService.add(SESSION_LIST);
        final List<Session> sessions = emptyService.findAll();
        Assert.assertEquals(SESSION_LIST.size()+countRecord, sessions.size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findAll(""));
        Assert.assertEquals(Collections.emptyList(), service.findAll(NON_EXISTING_SESSION_ID));
        @NotNull final List<Session> sessions  = service.findAll(USER_TEST.getId());
        for (Session session : sessions) {
            Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById("", USER_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_TEST.getId(), USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_SESSION_TEST.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.existsById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.existsById(null, USER_SESSION_TEST.getId()));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_SESSION_TEST.getId()));
    }

    @Test
    public void clear() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ISessionService emptyService = new SessionService(connectionService);
        emptyService.add(SESSION_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final ISessionService emptyService = new SessionService(connectionService);
        emptyService.add(SESSION_LIST);
        emptyService.removeAll();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeOne() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOne(null));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
        service.removeOne(session);
        Assert.assertNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertNull(service.removeOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
        service.removeOneById(session.getId());
        Assert.assertNull(service.findOneById(ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null, ADMIN_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(ADMIN_TEST.getId(), null));
        Assert.assertNull(service.removeOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId()));
        service.removeOneById(session.getId());
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION_TEST.getId()));
    }

}

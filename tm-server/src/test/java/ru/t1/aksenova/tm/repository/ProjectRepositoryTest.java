package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connection);

    @Before
    public void before() {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project.getId(), repository.findOneById(project.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());

    }

    @Test
    @Ignore
    public void createByUserId() {
        @NotNull final Project project = repository.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void set() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.set(ADMIN_PROJECT_LIST);
        final List<Project> projects2 = repository.findAll();
        projects2.forEach(project -> Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Project> projects, @NotNull final String id) {
        int index = 0;
        for (Project project : projects) {
            index++;
            if (id.equals(project.getId())) return index;
        }
        return -1;
    }

    @Test
    public void findAll() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        final List<Project> projects = emptyRepository.findAll();
        projects.forEach(project -> Assert.assertEquals(USER_TEST.getId(), project.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Project> projects = repository.findAll(USER_TEST.getId());
        projects.forEach(project -> Assert.assertEquals(USER_TEST.getId(), project.getUserId()));
    }

    @Test
    public void findAllComparator() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Project> projects = emptyRepository.findAll(USER_TEST.getId(), comparator);
        projects.forEach(project -> Assert.assertEquals(USER_TEST.getId(), project.getUserId()));
        final List<Project> projects2 = emptyRepository.findAll(ADMIN_TEST.getId(), comparator);
        projects2.forEach(project -> Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Project> projects = repository.findAll();
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Project> projects = repository.findAll(USER_TEST.getId());
        final int index = getIndexFromList(projects, USER_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        repository.removeAll();
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository(connection);
        emptyRepository.add(PROJECT_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Project project = repository.add(ADMIN_PROJECT1);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT1.getId()));
        repository.removeOne(project);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_PROJECT2.getId()));
        @Nullable final Project project = repository.add(ADMIN_PROJECT2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
        repository.removeOneById(project.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = repository.findAll();
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertNotNull(project);
        @Nullable final Project project2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(project2);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        repository.add(ADMIN_PROJECT1);
        @NotNull final List<Project> projects = repository.findAll(ADMIN_TEST.getId());
        final int index = getIndexFromList(projects, ADMIN_PROJECT1.getId());
        @Nullable final Project project2 = repository.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(project2);
        Assert.assertNull(repository.findOneById(project2.getId(), project2.getUserId()));
    }

}

package ru.t1.aksenova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository repository = new TaskRepository(connection);

    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task.getId(), repository.findOneById(task.getId()).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final Task task = repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
    }

    @Test
    public void createByUserId() {
        @NotNull final Task task = repository.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }

    @Test
    public void set() {
        repository.removeAll();
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.set(ADMIN_TASK_LIST);
        final List<Task> tasks = emptyRepository.findAll();
        tasks.forEach(task -> Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId()));
    }

    private int getIndexFromList(@NotNull final List<Task> tasks, @NotNull final String id) {
        int index = 0;
        for (Task task : tasks) {
            index++;
            if (id.equals(task.getId())) return index;
        }
        return -1;
    }

    @Test
    public void findAll() {
        repository.removeAll();
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        final List<Task> tasks = emptyRepository.findAll();
        tasks.forEach(task -> Assert.assertEquals(USER_TEST.getId(), task.getUserId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Task> tasks = repository.findAll(USER_TEST.getId());
        tasks.forEach(task -> Assert.assertEquals(USER_TEST.getId(), task.getUserId()));
    }

    @Test
    public void findAllComparator() {
        repository.removeAll();
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Task> tasks = emptyRepository.findAll(USER_TEST.getId(), comparator);
        tasks.forEach(task -> Assert.assertEquals(USER_TEST.getId(), task.getUserId()));
        final List<Task> tasks2 = emptyRepository.findAll(ADMIN_TEST.getId(), comparator);
        tasks2.forEach(task -> Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final List<Task> tasks = repository.findAll();
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIndexByUserId() {
        @NotNull final List<Task> tasks = repository.findAll(USER_TEST.getId());
        final int index = getIndexFromList(tasks, USER_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void removeAll() {
        repository.removeAll();
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(TASK_LIST);
        emptyRepository.removeAll();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final Task task = repository.add(ADMIN_TASK1);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK1.getId()));
        repository.removeOne(task);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertNull(repository.removeOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertNull(repository.removeOneById(ADMIN_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeOneById(null, ADMIN_TASK2.getId()));
        @Nullable final Task task = repository.add(ADMIN_TASK2);
        Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
        repository.removeOneById(task.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK2.getId()));
    }

    @Test
    public void removeOneByIndex() {
        repository.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = repository.findAll();
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        @Nullable final Task task = repository.findOneByIndex(index);
        Assert.assertEquals(ADMIN_TASK1.getId(), task.getId());
        Assert.assertNotNull(task);
        @Nullable final Task task2 = repository.removeOneByIndex(index);
        Assert.assertNotNull(task2);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeOneByIndexByUserId() {
        repository.add(ADMIN_TASK1);
        @NotNull final List<Task> tasks = repository.findAll(ADMIN_TEST.getId());
        final int index = getIndexFromList(tasks, ADMIN_TASK1.getId());
        @Nullable final Task task2 = repository.removeOneByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(task2);
        Assert.assertNull(repository.findOneById(task2.getId(), task2.getUserId()));
    }

    @Test
    public void findAllByProjectId() {
        repository.removeAll();
        @NotNull final ITaskRepository emptyRepository = new TaskRepository(connection);
        emptyRepository.add(USER_TASK_LIST);
        @Nullable final List<Task> tasks = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT1.getId());
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
        @Nullable final List<Task> tasks2 = emptyRepository.findAllByProjectId(USER_TASK1.getUserId(), USER_PROJECT2.getId());
        tasks2.forEach(task -> Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId()));
    }

}

package ru.t1.aksenova.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.component.ISaltProvider;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.util.HashUtil;

import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull String getTableName();

    @NotNull
    @SneakyThrows
    User fetch(@NotNull ResultSet row);

    @NotNull
    @Override
    @SneakyThrows
    User add(@NotNull User user);

    @NotNull
    default User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull ISaltProvider salt
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(salt, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull ISaltProvider salt);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role, @NotNull ISaltProvider salt);

    @NotNull
    @SneakyThrows
    User update(@NotNull User user);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}

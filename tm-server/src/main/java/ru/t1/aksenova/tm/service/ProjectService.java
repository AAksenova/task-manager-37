package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.html.HTMLTableRowElement;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IProjectService;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;

import java.sql.Connection;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

}

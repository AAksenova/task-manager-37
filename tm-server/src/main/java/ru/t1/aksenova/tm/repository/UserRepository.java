package ru.t1.aksenova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.component.ISaltProvider;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.field.EmailEmptyException;
import ru.t1.aksenova.tm.model.User;

import javax.security.auth.login.LoginException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private static final String tableName = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password_hash, email, first_name, last_name, middle_name, role, locked) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.isLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email,
            @NotNull final ISaltProvider salt
    ) {
        @NotNull final User user = create(login, password, salt);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final ISaltProvider salt
    ) {
        @NotNull final User user = create(login, password, salt);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password_hash = ?, email = ?, first_name = ?, last_name = ?," +
                        " middle_name = ?, role = ?, locked = ?  WHERE id = ? ", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.isLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (login == null) throw new LoginException();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        if (email == null) throw new EmailEmptyException();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login)!=null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email)!=null;
    }

}

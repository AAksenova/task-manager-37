package ru.t1.aksenova.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.model.Project;

import java.sql.ResultSet;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull String getTableName();

    @NotNull
    @SneakyThrows
    Project fetch(@NotNull ResultSet row);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    @SneakyThrows
    Project update(@NotNull Project project);
}

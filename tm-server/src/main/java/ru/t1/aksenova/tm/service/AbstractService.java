package ru.t1.aksenova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.IRepository;
import ru.t1.aksenova.tm.api.repository.IUserOwnedRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IService;
import ru.t1.aksenova.tm.exception.entity.ModelNotFoundException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.IndexIncorrectException;
import ru.t1.aksenova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aksenova.tm.model.AbstractModel;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.*;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final IConnectionService connectionService;

    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    public AbstractService(IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try ( @NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try ( @NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeOne(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        @Nullable M model;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.removeOneByIndex(index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

}

package ru.t1.aksenova.tm.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.endpoint.*;
import ru.t1.aksenova.tm.api.repository.*;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.dto.Domain;
import ru.t1.aksenova.tm.endpoint.*;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.SessionRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;
import ru.t1.aksenova.tm.repository.UserRepository;
import ru.t1.aksenova.tm.service.*;
import ru.t1.aksenova.tm.util.SystemUtil;
import sun.misc.BASE64Decoder;

import javax.xml.ws.Endpoint;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService, taskService, projectService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        createTableIfNotExist();
        initDemoData();
        loggerService.info("** WELCOME TO SERVER TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
       // initBackup();
    }

    private void stop() {
        backup.stop();
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initBackup() {
        backup.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void createTableIfNotExist() {
        final boolean initFlag = propertyService.getDatabaseInitialization();
        if (initFlag) {
            @NotNull final String fileName = propertyService.getDatabaseDampFile();
            if (!Files.exists(Paths.get(fileName))) return;
            List<String> lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            @NotNull String sql = String.join(System.lineSeparator(), lines);
            @NotNull final Connection connection = connectionService.getConnection();
            try {
                @NotNull final Statement statement = connection.createStatement();
                statement.executeUpdate(sql);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            } finally {
                connection.close();
            }
        }
    }

    private void initDemoData() {
        if (!userService.isLoginExist("test")) {
            @NotNull final User test = userService.create("test", "test", "test@test.ru");
            projectService.add(test.getId(), new Project("One test project", Status.IN_PROGRESS));
            projectService.add(test.getId(), new Project("Two test project", Status.NOT_STARTED));
            projectService.add(test.getId(), new Project("Three test project", Status.COMPLETED));
            taskService.add(test.getId(), new Task("Alfa task", Status.IN_PROGRESS));
            taskService.add(test.getId(), new Task("Beta task", Status.COMPLETED));
            taskService.add(test.getId(), new Task("Gamma task", Status.NOT_STARTED));
        }
        if (!userService.isLoginExist("user")) {
            @NotNull final User user = userService.create("user", "user", "user@user.ru");
            projectService.add(user.getId(), new Project("Six test project", Status.NOT_STARTED));
            taskService.add(user.getId(), new Task("Teuta task", Status.IN_PROGRESS));
        }
        if (!userService.isLoginExist("admin")) {
            @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
            projectService.add(admin.getId(), new Project("Four test project", Status.NOT_STARTED));
            projectService.add(admin.getId(), new Project("Five test project", Status.COMPLETED));
            taskService.add(admin.getId(), new Task("Delta task", Status.IN_PROGRESS));
            taskService.add(admin.getId(), new Task("Eta task", Status.NOT_STARTED));
        }
    }

}

package ru.t1.aksenova.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.model.Task;

import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull String getTableName();

    @NotNull
    @SneakyThrows
    Task fetch(@NotNull ResultSet row);

    @NotNull
    @SneakyThrows
    Task update(@NotNull Task task);

    @NotNull
    @SneakyThrows
    Task updateTaskProjectId(@NotNull Task task);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
